# README #

`is-host-port-open` is a RESTful microservice based on [OpenResty(ngx-lua)](http://openresty.org/) with [cosocket](http://wiki.nginx.org/HttpLuaModule#ngx.socket.tcp) within the docker used to check if a remote host:port is open or not.

### Usage and Syntax ###


```
#!bash

http://is-host-port-open.daoapp.io/<hostname_or_ip_addr>/<tcp_port_number>?timeout=<timeout_in_ms>

```

### Example ###


```
#!bash

$ curl http://is-host-port-open.daoapp.io/api.github.com/443
{"status":"OK","reason":"connected to api.github.com:443"}
```

### Limit ###
Please be noted the public service of `is-host-port-open.daoapp.io` is in China(`daocloud.io`) and it will have problem visiting some services like `google`, `facebook` or `twitter`.