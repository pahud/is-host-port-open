FROM pahud/openresty

MAINTAINER Pahud "pahudnet@gmail.com"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get -y upgrade

#ADD ./nginx.conf.d/nginx.conf /opt/openresty/nginx/conf/nginx.conf
ADD ./nginx.conf.d/default.conf /opt/openresty/nginx/conf/sites-enabled.d/
#ADD ./lua /opt/openresty/nginx/conf/lua
#ADD ./lualib /opt/openresty/nginx/conf/lualib

